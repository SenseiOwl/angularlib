﻿using AngularLib.BusinessLogic.Common;
using AngularLib.ViewModels;
using AngularLib.ViewModels.AuthorViewModels;
using AngularLib.ViewModels.BookViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.BusinessLogic.Services
{
    public class BookService
    {
        private BookRepository _repository;

        public BookService(string conn)
        {
            _repository = new BookRepository(conn);
        }

        public GetAllBookViewModel GetAll()
        {
            List<Book> books = _repository.GetAll();
            var result = new GetAllBookViewModel();
            result.Books = Mapper.Map<ICollection<Book>, ICollection<GetBookViewModel>>(books);
            return result;
        }

        public GetPaginatedBookViewModel GetPaginated(int page, int size)
        {
            List<Book> books = _repository.GetPaginatedBook(page,size);
            var result = new GetPaginatedBookViewModel();
            result.Books = Mapper.Map<ICollection<Book>, ICollection<GetBookViewModel>>(books);
            var pageInfo = new PageInfo();
            pageInfo.CurrentPage = page;
            pageInfo.ItemsPerPage = size;
            pageInfo.TotalItems = _repository.GetBooksCount();
            result.PageInfo = pageInfo;
            return result;
        }

        public GetBookViewModel Get(int id)
        {
            Book book = _repository.Get(id);
            if(book == null)
            {
                throw new BusinessLogicException("Can't find book!");
            }
            var result = Mapper.Map<Book,GetBookViewModel>(book);
            return result;
        }

        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public bool Update(UpdateBookViewModel model)
        {
            Book book = _repository.Get(model.Id);
            Mapper.Map(model, book);
            return _repository.Update(book);
        }

        public bool Create(CreateBookViewModel model)
        {
            Book book = new Book();
            Mapper.Map(model, book);
            return _repository.Create(book);
        }

    }
}
