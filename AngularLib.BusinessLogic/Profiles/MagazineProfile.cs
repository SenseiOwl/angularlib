﻿using AngularLib.ViewModels.MagazineViewModels;
using AngularLib.ViewModels.PublisherViewModels;
using AngularLib.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.BusinessLogic.Configs.Profiles
{
    public class MagazineProfile : Profile
    {
        public MagazineProfile()
        {
            CreateMap<Magazine, ShortMagazineViewModel>();

            CreateMap<Magazine, GetMagazineViewModel>()
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x => Mapper.Map<ICollection<PublisherMagazine>, ICollection<ShortPublisherViewModel>>(x.Publisher)));
            CreateMap<GetMagazineViewModel, Magazine>()
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherMagazine>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherMagazine = new PublisherMagazine();
                            publisherMagazine.PublisherId = publisher.Id;
                            publisherMagazine.MagazineId = x.Id;
                            result.Add(publisherMagazine);
                        }
                        return result;
                    }));
            

            CreateMap<CreateMagazineViewModel, Magazine>()
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherMagazine>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherMagazine = new PublisherMagazine();
                            publisherMagazine.PublisherId = publisher.Id;
                            result.Add(publisherMagazine);
                        }
                        return result;
                    }));

            CreateMap<UpdateMagazineViewModel, Magazine>()
                .ForMember(dest => dest.Publisher,
                    opt => opt.ResolveUsing(x =>
                    {
                        var result = new List<PublisherMagazine>();
                        foreach (var publisher in x.Publisher)
                        {
                            var publisherMagazine = new PublisherMagazine();
                            publisherMagazine.PublisherId = publisher.Id;
                            publisherMagazine.MagazineId = x.Id;
                            result.Add(publisherMagazine);
                        }
                        return result;
                    }));
        }
    }
}
