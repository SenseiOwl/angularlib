﻿using AngularLib.ViewModels.AuthorViewModels;
using AngularLib.ViewModels.BookViewModels;
using AngularLib.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.BusinessLogic.Configs.Profiles
{
    public class AuthorProfile : Profile
    {
        public AuthorProfile()
        {
            CreateMap<Author, ShortAuthorViewModel>();

            CreateMap<Author, GetAuthorViewModel>()
                .ForMember(dest => dest.Books, opt => opt.ResolveUsing(x => Mapper.Map<ICollection<AuthorBook>, ICollection<ShortBookViewModel>>(x.Books)));
            CreateMap<GetAuthorViewModel, Author>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Books, opt => opt.Ignore());

            CreateMap<CreateAuthorViewModel, Author>();

            CreateMap<UpdateAuthorViewModel, Author>();
        }
    }
}
