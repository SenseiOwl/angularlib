﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using AngularLib.Domain.Entities;

namespace AngularLib.DAL
{
    public class Context : DbContext
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Magazine> Magazines { get; set; }
        public DbSet<Newspaper> Newspapers { get; set; }

        public Context(DbContextOptions<Context> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthorBook>()
                .HasKey(t => new { t.AuthorId, t.BookId });
            modelBuilder.Entity<PublisherBook>()
                .HasKey(t => new { t.PublisherId, t.BookId });
            modelBuilder.Entity<PublisherMagazine>()
                .HasKey(t => new { t.PublisherId, t.MagazineId });
            modelBuilder.Entity<PublisherNewspaper>()
                .HasKey(t => new { t.PublisherId, t.NewspaperId });
        }

    }
}
