﻿using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularLib.DAL.Repositories
{
    public class AuthorRepository
    {
        private string _connectionString = null;

        public AuthorRepository(string conn)
        {
            _connectionString = conn;
        }

        public async Task<bool> Create(Author element)
        {
            #region SQLStrings
            var insertAuthorSql = "INSERT INTO Authors(Name,YearOfBirth,YearOfDeath) VALUES (@Name,@YearOfBirth,@YearOfDeath)";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    await db.QueryAsync(insertAuthorSql,
                        new { Name = element.Name, YearOfBirth = element.YearOfBirth, YearOfDeath = element.YearOfDeath });

                };
        }
            catch(Exception e)
            {
                //Log
                return false;
            }

            return true;
        }

        public async Task<bool> Delete(int id)
        {
            #region SQLStrings
            var numberDependentBooksSql = "SELECT COUNT(*) FROM AuthorBook WHERE AuthorId = @AuthorId";
            var dltAuthorSql = "DELETE FROM Authors WHERE Id = @AuthorId";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    var dependenceCount = 0;
                    dependenceCount += (await db.QueryAsync<int>(numberDependentBooksSql,
                        new { AuthorId = id })).FirstOrDefault();
                    if (dependenceCount > 0)
                    {
                        return false;
                    }
                    await db.QueryAsync(dltAuthorSql, new { AuthorId = id });

                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }


            return true;
        }

        public async Task<Author> Get(int id)
        {
            Author result;

            #region SQLStrings
            var authorSql = "SELECT * FROM Authors WHERE Id = @AuthorId;";
            var booksSql = "SELECT * FROM AuthorBook b JOIN Books p ON p.Id = b.BookId WHERE b.AuthorId = @AuthorId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = (await db.QueryAsync<Author>(authorSql, new { AuthorId = id })).FirstOrDefault();
                    if (result == null)
                    {
                        return null;
                    }
                    var books = (await db.QueryAsync<AuthorBook, Book, AuthorBook>(booksSql,
                        (authorBook, book) => { authorBook.Book = book; return authorBook; }, new { AuthorId = id }, splitOn: "AuthorId,BookId")).ToList();

                    result.Books = books;
                }
            }
            catch (Exception e)
            {
                //Log
                return null;
            }

            return result;
        }

        public async Task<List<Author>> GetAll()
        {
            var result = new List<Author>();

            #region SQLStrings
            var authorSql = "SELECT * FROM Authors";
            var booksSql = "SELECT * FROM AuthorBook b JOIN Authors a ON a.Id = b.AuthorId join Books as book on book.Id= b.BookId WHERE b.AuthorId in @AuthorId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = (await db.QueryAsync<Author>(authorSql)).ToList();
                    var books = (await db.QueryAsync<AuthorBook, Author, Book, AuthorBook>(booksSql,(authorBook, author, book) => { authorBook.Author = author; authorBook.Book = book; return authorBook; }, new { AuthorId = result.Select(x => x.Id) }, splitOn: "AuthorId,BookId,Id,Id")).ToList();
                    foreach (var author in result)
                    {
                        author.Books = books.Where(x=>x.AuthorId == author.Id).ToList();
                    }
                }
            }
            catch(Exception e)
            {
                //Log
                return result;
            }


            return result;
        }

        public async Task<List<Author>> GetPaginatedAuthors(int page, int size)
        {
            var result = new List<Author>();
            #region SQLStrings
            var authorSql = "SELECT * FROM Authors ORDER BY Id OFFSET @Offset ROW FETCH NEXT @Items ROWS ONLY";
            var booksSql = "SELECT * FROM AuthorBook b JOIN Authors a ON a.Id = b.AuthorId join Books as book on book.Id= b.BookId WHERE b.AuthorId in @AuthorId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = (await db.QueryAsync<Author>(authorSql, new { Offset = (page - 1) * size, Items = size })).ToList();
                    var books = (await db.QueryAsync<AuthorBook, Author, Book, AuthorBook>(booksSql, (authorBook, author, book) => { authorBook.Author = author; authorBook.Book = book; return authorBook; }, new { AuthorId = result.Select(x => x.Id) }, splitOn: "AuthorId,BookId,Id,Id")).ToList();
                    foreach (var author in result)
                    {
                        author.Books = books.Where(x => x.AuthorId == author.Id).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                //Log
                return result;
            }


            return result;
        }

        public async Task<bool> Update(Author element)
        {

            #region SQLStrings
            var updAuthorSql = "UPDATE Authors SET Name = @Name, YearOfBirth = @YearOfBirth, YearOfDeath = @YearOfDeath WHERE Authors.Id = @AuthorId";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    await db.QueryAsync(updAuthorSql,
                        new { Name = element.Name, YearOfBirth = element.YearOfBirth, YearOfDeath = element.YearOfDeath, AuthorId = element.Id });
                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;

        }

        public async Task<int> GetAuthorsCount()
        {
            int result;
            #region SQLStrings
            var countAuthorsSql = "SELECT COUNT(*) FROM Authors";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = (await db.QueryAsync<int>(countAuthorsSql)).FirstOrDefault();
                };
            }
            catch
            {
                return 0;
            }

            return result;
        }


    }
}


