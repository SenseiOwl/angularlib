﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.DAL.Repositories.Interfaces
{
    public interface IRepository<T> where T:class
    {
        bool Create(T element);
        bool Delete(int id);
        T Get(int id);
        List<T> GetAll();
        bool Update(T element);
    }
}
