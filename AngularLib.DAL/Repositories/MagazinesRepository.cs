﻿using AngularLib.Domain.Entities;
using AngularLib.DAL.Repositories.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace AngularLib.DAL.Repositories
{
    public class MagazinesRepository : IRepository<Magazine>
    {
        private string _connectionString = null;

        public MagazinesRepository(string conn)
        {
            _connectionString = conn;
        }

        public bool Create(Magazine element)
        {
            #region SQLStrings
            var insertMagazineSql = "INSERT INTO Magazines(Name,Type,YearOfPublishing) VALUES (@Name,@Type,@YearOfPublishing);SELECT SCOPE_IDENTITY();";
            var insertPublisherMagSql = "INSERT INTO PublisherMagazine (PublisherId, MagazineId) VALUES (@PublisherId, @MagazineId)";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    var magazineId = db.Query<int>(insertMagazineSql,
                        new { Name = element.Name, Type = (int)element.Type, YearOfPublishing = element.YearOfPublishing });

                    foreach (var publisher in element.Publisher)
                    {
                        db.Query(insertPublisherMagSql, new { PublisherId = publisher.PublisherId, MagazineId = magazineId });
                    }

                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;
        }

        public bool Delete(int id)
        {
            #region SQLStrings
            var dltFrmPublisherMagSql = "DELETE FROM PublisherMagazine WHERE MagazineId = @MagazineId";
            var dltFrmMagazinesSql = "DELETE FROM Magazines WHERE Id = @MagazineId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Query(dltFrmPublisherMagSql, new { MagazineId = id });
                    db.Query(dltFrmMagazinesSql, new { MagazineId = id });
                }
            }
            catch (Exception e)
            {
                //Log
                return false;
            }

            return true;

        }

        public Magazine Get(int id)
        {
            Magazine result;

            #region SQLStrings
            var magazinesSql = "SELECT * FROM Magazines WHERE Id = @MagazineId;";
            var publisherSql = "SELECT * FROM PublisherMagazine b JOIN Publishers p ON p.Id = b.PublisherId WHERE b.MagazineId = @MagazineId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Magazine>(magazinesSql, new { MagazineId = id }).FirstOrDefault();
                    if (result == null)
                    {
                        return null;
                    }
                    var publishers = db.Query<PublisherMagazine, Publisher, PublisherMagazine>(publisherSql,
                        (publisherMagazines, publisher) => { publisherMagazines.Publisher = publisher; return publisherMagazines; }, new { MagazineId = id }, splitOn: "PublisherId,MagazineId").ToList();

                    result.Publisher = publishers;
                }
            }
            catch (Exception e)
            {
                //Log
                return null;
            }

            return result;
        }

        public List<Magazine> GetAll()
        {
            var result = new List<Magazine>();

            #region SQLStrings
            var magazinesSql = "SELECT * FROM Magazines";
            var publisherSql = "SELECT * FROM PublisherMagazine b JOIN Publishers p ON p.Id = b.PublisherId join Magazines as magazine on magazine.Id= b.MagazineId WHERE b.MagazineId in @MagazineId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Magazine>(magazinesSql).ToList();
                    var publishers = db.Query<PublisherMagazine, Publisher, Magazine, PublisherMagazine>(publisherSql,
                            (publisherMagazine, publisher, magazine) => { publisherMagazine.Publisher = publisher; publisherMagazine.Magazine = magazine; return publisherMagazine; },
                            new { MagazineId = result.Select(x => x.Id) }, splitOn: "PublisherId,MagazineId,Id,Id").ToList();
                    foreach (var magazine in result)
                    {
                        magazine.Publisher = publishers.Where(x => x.MagazineId == magazine.Id).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                //Log
                return result;
            }

            return result;
        }

        public List<Magazine> GetPaginatedMagazines(int page, int size)
        {
            var result = new List<Magazine>();

            #region SQLStrings
            var magazinesSql = "SELECT * FROM Magazines ORDER BY Id OFFSET @Offset ROW FETCH NEXT @Items ROWS ONLY";
            var publisherSql = "SELECT * FROM PublisherMagazine b JOIN Publishers p ON p.Id = b.PublisherId join Magazines as magazine on magazine.Id= b.MagazineId WHERE b.MagazineId in @MagazineId";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<Magazine>(magazinesSql, new { Offset = (page - 1) * size, Items = size }).ToList();
                    var publishers = db.Query<PublisherMagazine, Publisher, Magazine, PublisherMagazine>(publisherSql,
                            (publisherMagazine, publisher, magazine) => { publisherMagazine.Publisher = publisher; publisherMagazine.Magazine = magazine; return publisherMagazine; },
                            new { MagazineId = result.Select(x => x.Id) }, splitOn: "PublisherId,MagazineId,Id,Id").ToList();
                    foreach (var magazine in result)
                    {
                        magazine.Publisher = publishers.Where(x => x.MagazineId == magazine.Id).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                //Log
                return result;
            }

            return result;
        }

        public bool Update(Magazine element)
        {
            #region SQLStrings
            var updateMagazineSql = "UPDATE Magazines SET Name = @Name,YearOfPublishing = @YearOfPublishing WHERE Magazines.Id = @MagazineId";
            var dltFrmPublisherMagSql = "DELETE FROM PublisherMagazine WHERE MagazineId = @MagazineId";
            var insertPublisherMagSql = "INSERT INTO PublisherMagazine (PublisherId, MagazineId) VALUES (@PublisherId, @MagazineId)";
            #endregion

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Query(updateMagazineSql,
                        new { Name = element.Name, Type = (int)element.Type, YearOfPublishing = element.YearOfPublishing, MagazineId = element.Id });

                    db.Query(dltFrmPublisherMagSql, new { MagazineId = element.Id });

                    foreach (var publisher in element.Publisher)
                    {
                        db.Query(insertPublisherMagSql, new { PublisherId = publisher.PublisherId, MagazineId = element.Id });
                    }

                };
            }
            catch (Exception e)
            {
                //Log
                return false;
            }
            return true;

        }

        public int GetMagazinesCount()
        {
            int result;
            #region SQLStrings
            var countMagazinesSql = "SELECT COUNT(*) FROM Magazines";
            #endregion
            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    result = db.Query<int>(countMagazinesSql).FirstOrDefault();
                };
            }
            catch
            {
                return 0;
            }


            return result;
        }
    }
}
