import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Publisher } from '../../../viewModels/publisherViewModels/publisherView';
import { PublisherService } from '../../../services/publisher.service';

@Component({
    selector: 'app-publisher',
    templateUrl: './publisher.component.html',
    styleUrls: ['./publisher.component.css']
})
export class PublisherComponent {
    publisher: Publisher;
    selectedBookId: number;
    selectedMagazineId: number;
    selectedNewspaperId: number;

    constructor(
        private _route: ActivatedRoute,
        private _publisherService: PublisherService,
        private _location: Location
    ) { }

    ngOnInit(): void {
        this.getPublisher();
    }

    onMagazineClick(id: number) {
        this.selectedMagazineId = id;
    }

    onBookClick(id: number) {
        this.selectedBookId = id;
    }

    onNewspaperClick(id: number) {
        this.selectedNewspaperId = id;
    }

    getPublisher(): void {
        const id = this._route.snapshot.params['id'];
        this._publisherService.getPublisherById(id)
            .subscribe(publisher => this.publisher = publisher);
    }

    goBack(): void {
        this._location.back();
    }
}
