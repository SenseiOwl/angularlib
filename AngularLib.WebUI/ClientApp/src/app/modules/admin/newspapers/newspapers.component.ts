import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { PaginatedNewspaper } from '../../../viewModels/newspaperViewModels/paginatedNewspaper';
import { NewspaperService } from '../../../services/newspaper.service';


@Component({
    selector: 'ad-newspapers',
    templateUrl: './newspapers.component.html'
})
export class AdNewspapersComponent {
    public newspapers: PaginatedNewspaper;

    itemPerPage: number = 5;
    private _currentPage: number;

    constructor(private _newspaperService: NewspaperService) {

    }

    ngOnInit(): void {
        this.getNewspapers(this.CurrentPage);
    }

    getNewspapers(page: number) {
        this._newspaperService.getPaginatedNewspapers(page, this.itemPerPage)
            .subscribe(result => {
                this.newspapers = result;
            }, error => console.error(error));
    }

    delete(ID: any) {
        var ans = confirm("Do you want to delete customer with Id: " + ID);
        if (ans) {
            this._newspaperService.deleteNewspaper(ID).subscribe((data) => {
                this.getNewspapers(this.CurrentPage);
            }, error => console.error(error))
        }
    }

    get CurrentPage() {
        if (this._currentPage < 1 || this._currentPage == undefined) {
            return 1;
        }
        else {
            return this._currentPage;
        }
    }
    set CurrentPage(page: number) {
        this._currentPage = page;
        this.getNewspapers(this.CurrentPage);
    }

    onChange(itemPerPage: number) {
        this.itemPerPage = itemPerPage;
        this.getNewspapers(this.CurrentPage);
    }

    goPrevious() {
        this.CurrentPage--;
    }

    goNext() {
        this.CurrentPage++;
    }
}



