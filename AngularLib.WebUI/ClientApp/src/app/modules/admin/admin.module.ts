import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routing } from './admin.routing';
import { AdNewspapersComponent } from './newspapers/newspapers.component';
import { AdBooksComponent } from './books/books.component';
import { AdMagazinesComponent } from './magazines/magazines.component';
import { AdPublishersComponent } from './publishers/publishers.component';
import { AdAuthorsComponent } from './authros/authors.component';
import { AdminComponent } from './admin/admin.component';
import { AdAuthorChangerComponent } from './authorchanger/authorchanger.component';
import { AdPublisherChangerComponent } from './publisherchanger/publisherchanger.component';
import { AdBookChangerComponent } from './bookchanger/bookchanger.component';
import { AdNewspaperChangerComponent } from './newspaperchanger/newspaperchanger.component';
import { AdMagazineChangerComponent } from './magazinechanger/magazinechanger.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { GridModule } from '@progress/kendo-angular-grid';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';




@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      SharedModule,
      routing,
      ButtonsModule,
      GridModule,
      BrowserAnimationsModule,
      DropDownsModule
    ],
    declarations: [AdNewspapersComponent,
        AdBooksComponent,
        AdMagazinesComponent,
        AdPublishersComponent,
        AdAuthorsComponent,
        AdminComponent,
        AdAuthorChangerComponent,
        AdPublisherChangerComponent,
        AdBookChangerComponent,
        AdNewspaperChangerComponent,
        AdMagazineChangerComponent,       
    ],
    providers: []
})
export class AdminModule { }
