import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Magazine } from '../../../viewModels/magazineViewModels/magazineView';
import { ShortPublisher } from '../../../viewModels/publisherViewModels/shortPublisherView';
import { MagazineService } from '../../../services/magazine.service';
import { PublisherService } from '../../../services/publisher.service';


@Component({
    selector: 'ad-magazinechanger',
    templateUrl: './magazinechanger.component.html',
})
export class AdMagazineChangerComponent implements OnInit {
    title: string = "Create";
    id: number;
    errorMessage: any;
    isRequesting: boolean;
    magazine: Magazine;

    publishers: ShortPublisher[];

    constructor(private _avRoute: ActivatedRoute,
        private _magazineService: MagazineService,
        private _publisherService: PublisherService,
        private _router: Router) {

        if (this._avRoute.snapshot.params["id"]) {
            this.id = this._avRoute.snapshot.params["id"];
        }

        this.magazine = new Magazine();

    }

    ngOnInit() {
        if (this.id > 0) {
            this.title = "Edit";
            this._magazineService.getMagazineById(this.id)
                .subscribe(resp => this.magazine = resp
                , error => this.errorMessage = error);
        }

        this._publisherService.getShortPublishers()
            .subscribe(publishers => this.publishers = publishers,
            error => this.errorMessage = error);


    }

    save() {
        this.isRequesting = true;
        if (this.title == "Create") {
            this._magazineService.saveMagazine(this.magazine).finally(() => this.isRequesting = false)
                .subscribe(value => {
                    this._router.navigate(['/admin/magazines']);
                }, error => this.errorMessage = error)
        }
        else if (this.title == "Edit") {
            this._magazineService.updateMagazine(this.magazine).finally(() => this.isRequesting = false)
                .subscribe(value => {
                    this._router.navigate(['/admin/magazines']);
                }, error => this.errorMessage = error)

        }
    }

    cancel() {
        this._router.navigate(['/admin/magazines']);

    }

}


