import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { PaginatedPublisher } from '../../../viewModels/publisherViewModels/paginatedPublisher';
import { PublisherService } from '../../../services/publisher.service';

@Component({
    selector: 'ad-publishers',
    templateUrl: './publishers.component.html'
})
export class AdPublishersComponent {
    public publishers: PaginatedPublisher;
    itemPerPage: number = 5;
    private _currentPage: number;

    constructor(private _publisherService: PublisherService) {
        
    }

    ngOnInit() {
        this.getPublishers(this.CurrentPage);
    }

    getPublishers(page: number) {
        this._publisherService.getPaginatedPublishers(page, this.itemPerPage).subscribe(result => {
            this.publishers = result;
        }, error => console.error(error));
    }

    delete(ID: any) {
        var ans = confirm("Do you want to delete customer with Id: " + ID);
        if (ans) {
            this._publisherService.deletePublisher(ID).subscribe((data) => {
                this.getPublishers(this.CurrentPage);
            }, error => console.error(error))
        }
    }


    get CurrentPage() {
        if (this._currentPage < 1 || this._currentPage == undefined) {
            return 1;
        }
        else {
            return this._currentPage;
        }
    }
    set CurrentPage(page: number) {
        this._currentPage = page;
        this.getPublishers(this.CurrentPage);
    }

    onChange(itemPerPage: number) {
        this.itemPerPage = itemPerPage;
        this.getPublishers(this.CurrentPage);
    }

    goPrevious() {
        this.CurrentPage--;
    }

    goNext() {
        this.CurrentPage++;
    }
}