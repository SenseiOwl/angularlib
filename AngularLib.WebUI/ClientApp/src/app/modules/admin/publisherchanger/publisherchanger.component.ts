import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Publisher } from '../../../viewModels/publisherViewModels/publisherView';
import { PublisherService } from '../../../services/publisher.service';



@Component({
    selector: 'ad-publisherchanger',
    templateUrl: './publisherchanger.component.html',
})
export class AdPublisherChangerComponent implements OnInit {
    title: string = "Create";
    id: number;
    errorMessage: any;
    publisher: Publisher;
    isRequesting: boolean;
    constructor(private _avRoute: ActivatedRoute,
        private _publisherService: PublisherService, private _router: Router) {
        if (this._avRoute.snapshot.params["id"]) {
            this.id = this._avRoute.snapshot.params["id"];
        }

        this.publisher = new Publisher();

    }

    ngOnInit() {
        if (this.id > 0) {
            this.title = "Edit";
            this._publisherService.getPublisherById(this.id)
                .subscribe(resp => this.publisher = resp
                , error => this.errorMessage = error);
        }
    }

    save() {
        this.isRequesting = true;
        if (this.title == "Create") {
            this._publisherService.savePublisher(this.publisher).finally(() => this.isRequesting = false)
                .subscribe(value => {
                    this._router.navigate(['/admin/publishers']);
                }, error => this.errorMessage = error)
        }
        else if (this.title == "Edit") {
            this._publisherService.updatePublisher(this.publisher).finally(() => this.isRequesting = false)
                .subscribe(value => {
                    this._router.navigate(['/admin/publishers']);
                }, error => this.errorMessage = error)
        }
    }

    cancel() {
        this._router.navigate(['/admin/publishers']);


    }
}

