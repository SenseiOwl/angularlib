﻿export class PageInfo {

    public totalItems: number;
    public itemsPerPage: number;
    public currentPage: number;
    public totalPages: number;

}