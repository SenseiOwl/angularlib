﻿import { ShortBook } from "../bookViewModels/shortBookView";


export class Author {
    id: number;
    name: string;
    yearOfBirth: number;
    yearOfDeath: any;
    books: ShortBook[];
}