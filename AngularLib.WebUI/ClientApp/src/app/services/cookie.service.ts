﻿import { Injectable, Inject } from '@angular/core';


@Injectable()
export class CookieService {

    constructor() {
       
    }

    getCookie(name: string) : string | undefined {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    setCookie(name : string , value : string, expires : number) {

        var date = new Date(new Date().getTime() + expires * 1000);
        var updatedCookie = name + "=" + value + "; path=/;" + "expires=" + date.toUTCString();

        document.cookie = updatedCookie;
    }

    deleteCookie(name : string) {
        this.setCookie(name, "", -1);
    }

    
}