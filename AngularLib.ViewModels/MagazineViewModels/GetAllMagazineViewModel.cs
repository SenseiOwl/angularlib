﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.MagazineViewModels
{
    public class GetAllMagazineViewModel
    {
        public ICollection<GetMagazineViewModel> Magazines { get; set; }
        public GetAllMagazineViewModel()
        {
            Magazines = new List<GetMagazineViewModel>();
        }
    }
}
