﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.MagazineViewModels
{
    public class GetPaginatedMagazineViewModel
    {
        public ICollection<GetMagazineViewModel> Magazines { get; set; }
        public PageInfo PageInfo { get; set; }

        public GetPaginatedMagazineViewModel()
        {
            Magazines = new List<GetMagazineViewModel>();
        }
    }
}
