﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.AuthorViewModels
{
    public class GetAllAuthorViewModel
    {
        public ICollection<GetAuthorViewModel> Authors { get; set; }
        
        public GetAllAuthorViewModel()
        {
            Authors = new List<GetAuthorViewModel>();
        }
    
    }
}
