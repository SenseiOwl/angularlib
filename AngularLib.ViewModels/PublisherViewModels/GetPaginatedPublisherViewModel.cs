﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.ViewModels.PublisherViewModels
{
    public class GetPaginatedPublisherViewModel
    {
        public ICollection<GetPublisherViewModel> Publishers { get; set;}
        public PageInfo PageInfo { get; set;}

        public GetPaginatedPublisherViewModel()
        {
            Publishers = new List<GetPublisherViewModel>();
        }
    }
}
