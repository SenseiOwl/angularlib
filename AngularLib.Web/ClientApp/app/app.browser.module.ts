import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AuthorService } from './services/author.service';
import { BookService } from './services/book.service';
import { ConfigService } from './services/config.service';
import { CookieService } from './services/cookie.service';
import { MagazineService } from './services/magazine.service';
import { NewspaperService } from './services/newspaper.service';
import { PublisherService } from './services/publisher.service';
import { UserService } from './services/user.service';
import { AuthGuard } from './common/auth.guard';
import { HomeComponent } from './components/home/home.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { routing } from './app.routing';
import { AccountModule } from './modules/account/account.module';
import { LibraryModule } from './modules/library/library.module';
import { AdminModule } from './modules/admin/admin.module';
import { SharedModule } from './modules/shared/shared.module';



@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AccountModule,
        LibraryModule,
        AdminModule,
        SharedModule.forRoot(),
        routing
    ],
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl }
    ]
})
export class AppModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
