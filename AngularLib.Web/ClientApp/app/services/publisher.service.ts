﻿import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { PaginatedPublisher } from '../viewModels/publisherViewModels/paginatedPublisher';
import { AllPublisher } from '../viewModels/publisherViewModels/allPublisherView';
import { ShortPublisher } from '../viewModels/publisherViewModels/shortPublisherView';
import { Publisher } from '../viewModels/publisherViewModels/publisherView';
import { BaseService } from './base.service';
import { UserService } from './user.service';

@Injectable()
export class PublisherService extends BaseService {
    myAppUrl: string = "";

    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string, private _user : UserService) {
        super();
        this.myAppUrl = baseUrl;
    }

    getPublishers(): Observable<AllPublisher> {
        return this._http.get(this.myAppUrl + 'api/Publisher/GetAll')
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getShortPublishers(): Observable<ShortPublisher[]> {
        return this._http.get(this.myAppUrl + 'api/Publisher/GetShortPublishers')
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getPaginatedPublishers(page: number, size: number): Observable<PaginatedPublisher> {
        return this._http.get(this.myAppUrl + "api/Publisher/GetPaginated/" + page + "/" + size)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    getPublisherById(id: number): Observable<Publisher> {
        return this._http.get(this.myAppUrl + "api/Publisher/Get/" + id)
            .map((response: Response) => response.json())
            .catch(this.handleError)
    }

    savePublisher(author: any): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.post(this.myAppUrl + 'api/Publisher/Create', author)
            .map((response: Response) => response.json())
            .catch(this.handleError)
    }

    updatePublisher(author: any): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.put(this.myAppUrl + 'api/Publisher/Edit', author)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    deletePublisher(id: number): Observable<boolean> {
        let headers: Headers = this._user.getTokenHeader();
        return this._http.delete(this.myAppUrl + "api/Publisher/Delete/" + id)
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

}