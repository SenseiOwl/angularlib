﻿import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { AuthorService } from "../author.service";
import { BookService } from "../book.service";
import { PublisherService } from "../publisher.service";
import { NewspaperService } from "../newspaper.service";
import { MagazineService } from "../magazine.service";
import { UserService } from "../user.service";
import { ConfigService } from "../config.service";
import { PaginationComponent } from "../../modules/shared/pagination/pagination.component";


@NgModule({
    imports: [
        CommonModule
    ],
    declarations:
        [
            PaginationComponent
        ],

    exports:
        [
            PaginationComponent
        ],
    providers: [
        AuthorService,
        BookService,
        PublisherService,
        NewspaperService,
        MagazineService,
        UserService,
        ConfigService
    ]
})
export class ServiceModule { }