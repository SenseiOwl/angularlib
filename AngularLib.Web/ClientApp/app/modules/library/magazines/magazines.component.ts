import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { PaginatedMagazine } from '../../../viewModels/magazineViewModels/paginatedMagazineView';
import { Magazine } from '../../../viewModels/magazineViewModels/magazineView';
import { MagazineService } from '../../../services/magazine.service';

@Component({
    selector: 'magazines',
    templateUrl: './magazines.component.html',
    styleUrls: ['magazines.component.css']
})
export class MagazinesComponent {
    public magazines: PaginatedMagazine;
    selectedMagazine: Magazine;


    itemPerPage: number = 5;
    private _currentPage: number;

    constructor(private _magazineService: MagazineService) {

    }

    ngOnInit() {
        this.getMagazines(this.CurrentPage);
    }

    getMagazines(page:number) {

        this._magazineService.getPaginatedMagazines(page, this.itemPerPage).subscribe(
            result => {
                this.magazines = result;
            }, error => console.error(error));
    }

    onClick(magazine: Magazine) {
        this.selectedMagazine = magazine;
    }

    get CurrentPage() {
        if (this._currentPage < 1 || this._currentPage == undefined) {
            return 1;
        }
        else {
            return this._currentPage;
        }
    }
    set CurrentPage(page: number) {
        this._currentPage = page;
        this.getMagazines(this.CurrentPage);
    }

    onChange(itemPerPage: number) {
        this.itemPerPage = itemPerPage;
        this.getMagazines(this.CurrentPage);
    }

    goPrevious() {
        this.CurrentPage--;
    }

    goNext() {
        this.CurrentPage++;
    }
    
}



