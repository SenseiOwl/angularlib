﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { routing } from './account.routing';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { EmailValidator } from '../../directives/email.validator.directive';
import { myFocus } from '../../directives/focus.directive';
import { SharedModule } from '../shared/shared.module';


@NgModule({
    imports: [
        CommonModule, FormsModule, SharedModule,  routing
    ],
    declarations: [RegistrationFormComponent, EmailValidator, LoginFormComponent, myFocus],
})
export class AccountModule { }