﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AngularLib.BusinessLogic.Services;
using AngularLib.ViewModels.NewspaperViewModels;
using Microsoft.AspNetCore.Authorization;
using AngularLib.BusinessLogic.Common;

namespace AngularLib.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class NewspaperController : Controller
    {
        private NewspaperService _service;

        public NewspaperController(NewspaperService service)
        {
            _service = service;
        }


        [HttpGet("[action]")]
        public IActionResult GetAll()
        {
            try
            {
                var result = _service.GetAll();
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }

        [HttpGet("[action]/{page}/{size}")]
        public IActionResult GetPaginated(int page, int size)
        {
            try
            {
                var result = _service.GetPaginated(page, size);
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }

        [HttpGet("[action]/{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var result = _service.Get(id);
                return Ok(result);
            }
            catch (BusinessLogicException exception)
            {
                return BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }
        [Authorize(Policy = "ApiAdmin")]
        [HttpPost("[action]")]
        public IActionResult Create([FromBody]CreateNewspaperViewModel magazine)
        {
            try
            {
                var result = _service.Create(magazine);
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }
        [Authorize(Policy = "ApiAdmin")]
        [HttpPut("[action]")]
        public IActionResult Edit([FromBody]UpdateNewspaperViewModel magazine)
        {
            try
            {
                var result = _service.Update(magazine);
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }
        [Authorize(Policy = "ApiAdmin")]
        [HttpDelete("[action]/{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var result = _service.Delete(id);
                return Ok(result);
            }
            catch (Exception exception)
            {
                //Log information
                return BadRequest();
            }
        }
    }
}