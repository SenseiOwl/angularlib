﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AngularLib.WebUI.Authentication;
using Microsoft.AspNetCore.Identity;
using AngularLib.WebUI.Authentication.Models;
using AutoMapper;
using AngularLib.ViewModels.AuthViewModels;
using AngularLib.WebUI.Authentication.Helpers;

namespace AngularLib.WebUI.Controllers
{
    [Route("api/[controller]")]
    public class AccountsController : Controller
    {
        private readonly ApplicationDbContext _appDbContext;
        private readonly UserManager<AppUser> _userManager;

        public AccountsController(UserManager<AppUser> userManager, ApplicationDbContext appDbContext)
        {
            _userManager = userManager;
            _appDbContext = appDbContext;
        }

        // POST api/accounts/post
        [HttpPost("[action]")]
        public async Task<IActionResult> Post([FromBody]RegistrationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userIdentity = new AppUser();
            userIdentity.UserName = model.Email;
            userIdentity.FirstName = model.FirstName;
            userIdentity.LastName = model.LastName;
            userIdentity.Role = Constants.Strings.JwtClaims.ApiAdmin;            

            IdentityResult result = await _userManager.CreateAsync(userIdentity, model.Password);

            if (!result.Succeeded)
            {
                return new BadRequestObjectResult(Errors.AddErrorsToModelState(result, ModelState));
            }
                
            await _appDbContext.BaseUsers.AddAsync(new BaseUser { IdentityId = userIdentity.Id, Location = model.Location });
            await _appDbContext.SaveChangesAsync();

            return new OkObjectResult("Account created");
        }
    }
}