﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.Domain.Entities
{
    public class PublisherNewspaper
    {
        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }

        public int NewspaperId { get; set; }
        public Newspaper Newspaper { get; set; }
    }
}
