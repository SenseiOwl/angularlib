﻿using AngularLib.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularLib.Domain.Entities
{
    public class Magazine
    {
        public PublicationType Type { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }

        public int YearOfPublishing { get; set; }

        public virtual ICollection<PublisherMagazine> Publisher { get; set; }

        public Magazine()
        {
            Type = PublicationType.Magazine;

            Publisher = new List<PublisherMagazine>();
        }
    }
}
